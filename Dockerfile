FROM golang:1.14 as build
RUN apt-get install ca-certificates -y

WORKDIR /go/src/app
COPY . .

RUN GOOS=linux GARCH=amd64 go build ./cmd/emet-selch
RUN mv emet-selch emet-selch.bin

#
# Run
#
FROM ubuntu:20.04

COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /go/src/app/emet-selch.bin /srv/emet-selch

CMD ["/srv/emet-selch"]
