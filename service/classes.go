package service

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/ekkosoft/emet-selch/command/classes"
	repo "gitlab.com/ekkosoft/emet-selch/db"
	goLodestone "gitlab.com/ekkosoft/go-lodestone"
)

func ClassesGetIdService(db *gorm.DB) classes.GetSavedCharacterId {
	return func(userId string, isAlt bool) (string, error) {
		user := &repo.UserCharacter{DiscordId: userId}
		if db.First(user).Error != nil {
			return "", fmt.Errorf("no character stored")
		}
		if isAlt {
			if user.LodestoneAltId == "" {
				return "", fmt.Errorf("no character stored")
			}
			return user.LodestoneAltId, nil
		}
		if user.LodestoneId == "" {
			return "", fmt.Errorf("no character stored")
		}
		return user.LodestoneId, nil
	}
}

func ClassesLodestoneService(ls *goLodestone.Lodestone) classes.GetLodestoneData {
	return func(lodestoneId string) (*classes.Character, error) {
		res, err := ls.GetCharacterProfile(lodestoneId)
		if err != nil {
			return nil, fmt.Errorf("get profile service failed: %v", err)
		}
		char := &classes.Character{
			ID:           res.ID,
			FullName:     res.FullName,
			FullPortrait: res.FullPortrait,
			Tanks:        make(map[string]string,0),
			Healers:      make(map[string]string,0),
			DPS:          make(map[string]string,0),
			DOH:          make(map[string]string,0),
			DOL:          make(map[string]string,0),
		}
		char.Tanks["PLD"] = res.Classes.PLD
		char.Tanks["WAR"] = res.Classes.WAR
		char.Tanks["DRK"] = res.Classes.DRK
		char.Tanks["GNB"] = res.Classes.GNB
		char.Healers["WHM"] = res.Classes.WHM
		char.Healers["SCH"] = res.Classes.SCH
		char.Healers["AST"] = res.Classes.AST
		char.DPS["MNK"] = res.Classes.MNK
		char.DPS["DRG"] = res.Classes.DRG
		char.DPS["NIN"] = res.Classes.NIN
		char.DPS["SAM"] = res.Classes.SAM
		char.DPS["BRD"] = res.Classes.BRD
		char.DPS["MCH"] = res.Classes.MCH
		char.DPS["DNC"] = res.Classes.DNC
		char.DPS["BLM"] = res.Classes.BLM
		char.DPS["ACN"] = res.Classes.ACN
		char.DPS["RDM"] = res.Classes.RDM
		char.DPS["BLU"] = res.Classes.BLU
		char.DOH["CRP"] = res.Classes.CRP
		char.DOH["BSM"] = res.Classes.BSM
		char.DOH["ARM"] = res.Classes.ARM
		char.DOH["GSM"] = res.Classes.GSM
		char.DOH["LTW"] = res.Classes.LTW
		char.DOH["WVR"] = res.Classes.WVR
		char.DOH["ALC"] = res.Classes.ALC
		char.DOH["CUL"] = res.Classes.CUL
		char.DOL["MIN"] = res.Classes.MIN
		char.DOL["BTN"] = res.Classes.BTN
		char.DOL["FSH"] = res.Classes.FSH
		return char, nil
	}
}
