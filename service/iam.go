package service

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
	"gitlab.com/ekkosoft/emet-selch/command/iam"
	repo "gitlab.com/ekkosoft/emet-selch/db"
	goLodestone "gitlab.com/ekkosoft/go-lodestone"
)

func IamNameService(ls *goLodestone.Lodestone) iam.FindByNameService {
	return func(world string, name string, surname string) (*iam.Character, error) {
		cs, err := ls.CharacterSearch(world, name, surname)
		if err != nil || len(cs) < 1 {
			return nil, fmt.Errorf("error searching lodestone: %v", err)
		}
		for _, char := range cs {
			if strings.ToLower(char.Name) == strings.ToLower(name) ||
				strings.ToLower(char.Surname) == strings.ToLower(surname) {
				return &iam.Character{
					ID:          char.ID,
					Name:        char.Name,
					Surname:     char.Surname,
					World:       char.World,
					FreeCompany: char.FreeCompany,
					Portrait:    char.FacePortrait,
				}, nil
			}
		}
		return nil, fmt.Errorf("didn't find character: %v", err)
	}
}

func IamSaveService(db *gorm.DB) iam.SaveCharacter {
	return func(userId string, lodestoneId string, alt bool) error {
		user := &repo.UserCharacter{DiscordId: userId}
		db.FirstOrCreate(user)
		if alt {
			user.LodestoneAltId = lodestoneId
		} else {
			user.LodestoneId = lodestoneId
		}
		return db.Save(user).Error
	}
}