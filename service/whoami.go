package service

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/ekkosoft/emet-selch/command/whoami"
	repo "gitlab.com/ekkosoft/emet-selch/db"
	goLodestone "gitlab.com/ekkosoft/go-lodestone"
)

func WhoamiGetIdService(db *gorm.DB) whoami.GetSavedCharacterId {
	return func(userId string, isAlt bool) (string, error) {
		user := &repo.UserCharacter{DiscordId: userId}
		if db.First(user).Error != nil {
			return "", fmt.Errorf("no character stored")
		}
		if isAlt {
			if user.LodestoneAltId == "" {
				return "", fmt.Errorf("no character stored")
			}
			return user.LodestoneAltId, nil
		}
		if user.LodestoneId == "" {
			return "", fmt.Errorf("no character stored")
		}
		return user.LodestoneId, nil
	}
}

func WhoamiLodestoneService(ls *goLodestone.Lodestone) whoami.GetLodestoneData {
	return func(lodestoneId string) (*whoami.Character, error) {
		res, err := ls.GetCharacterProfile(lodestoneId)
		if err != nil {
			return nil, fmt.Errorf("get profile service failed: %v", err)
		}
		char := &whoami.Character{
			ID:           res.ID,
			FacePortrait: res.FacePortrait,
			Title:        res.Title,
			FullName:     res.FullName,
			World:        res.World,
			Race:         res.Race,
			Clan:         res.Clan,
			Gender:       res.Gender,
			Nameday:      res.Nameday,
			Guardian:     res.Guardian,
			CityState:    res.CityState,
			GrandCompany: res.GrandCompany,
			FreeCompany:  res.FreeCompany,
			FullPortrait: res.FullPortrait,
		}
		return char, nil
	}
}
