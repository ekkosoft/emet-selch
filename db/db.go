package db

import (
	"fmt"
	"os"
	"time"

	"github.com/jinzhu/gorm"
)

func NewDatabaseConnection() *gorm.DB {
	host := "host=" + os.Getenv("POSTGRES_HOST")
	port := "port=5432"
	user := "user=" + os.Getenv("POSTGRES_USER")
	name := "dbname=" + os.Getenv("POSTGRES_DB")
	pass := "password=" + os.Getenv("POSTGRES_PASSWORD")
	var db *gorm.DB
	var err error
	var connection bool
	for connecting := true; connecting; connecting = !connection {
		db, err = gorm.Open("postgres", host+" "+port+" "+user+" "+name+" "+pass+" sslmode=disable")
		if err != nil {
			fmt.Printf("db connection failed, waiting: %v\n", err)
			time.Sleep(time.Second * 10)
		} else {
			connection = true
		}
	}
	return db
}
