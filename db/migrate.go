package db

import "github.com/jinzhu/gorm"

var entities = []interface{}{
	&UserCharacter{},
}

func AutoMigrate(db *gorm.DB, rebuild bool) {
	if rebuild {
		db.DropTable(entities...)
	}
	db.AutoMigrate(entities...)
}