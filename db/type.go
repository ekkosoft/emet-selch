package db

type UserCharacter struct {
	DiscordId string `gorm:"primary_key"`
	LodestoneId string
	LodestoneAltId string
}