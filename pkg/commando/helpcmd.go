package commando

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

type HelpCmd struct {
	ICommand
	params ParamDefinition
	bot *Bot
}

func NewHelpCmd(bot *Bot) *HelpCmd {
	return &HelpCmd{bot: bot, params: ParamDefinition{}}
}

func (c *HelpCmd) Name() string {
	return "help"
}

func (c *HelpCmd) Description() string {
	return "Help with bot commands."
}

func (c *HelpCmd) Params() ParamDefinition {
	return c.params
}

func (c *HelpCmd) Invoke(ctx *CommandContext) (*discordgo.MessageEmbed, error) {
	if len(ctx.Params["_raw"]) > 0 {
		return helpCmdDetails(c.bot, ctx.Params["_raw"])
	} else {
		return helpCmdList(c.bot)
	}
}

func helpCmdList(b *Bot) (*discordgo.MessageEmbed, error) {
	cmds := make([]string, 0)
	for _, cmd := range b.commandRegistry {
		if cmd.Name() == "help" { continue }
		desc := ""
		if has, ok := interface{}(cmd).(ICommandDescription); ok {
			desc = " - *" + has.Description() + "*"
		}
		cmds = append(cmds, "`" + cmd.Name() + "`" + desc)
	}
	return &discordgo.MessageEmbed{
		Title: "Command Help",
		Description: "Commands are triggered by using the invocation `" + b.Invocation + "`.\n" +
			"To use a command, type your message like this:\n" +
			"`" + b.Invocation + " command arguments...`\n" +
			"Below you can find a list of commands available:",
		Fields: []*discordgo.MessageEmbedField{
			{
				Name: "Help Commands",
				Value: "`help` - Creates this help message\n`help command` - Shows details about a *command*",
			},
			{
				Name: "Available Commands",
				Value: strings.Join(cmds, "\n"),
			},
		},
	}, nil
}

func helpCmdDetails(b *Bot, cmdName string) (*discordgo.MessageEmbed, error) {
	cmdName = strings.Trim(cmdName, " ")
	if cmdName == "help" { return helpCmdList(b) }
	if cmd, ok := b.commandRegistry[cmdName]; ok {
		res := &discordgo.MessageEmbed{Title: cmd.Name(), Fields: []*discordgo.MessageEmbedField{}}
		if has, ok := interface{}(cmd).(ICommandDescription); ok {
			res.Description = has.Description()
		}
		if has, ok := interface{}(cmd).(ICommandExample); ok {
			res.Fields = append(res.Fields, &discordgo.MessageEmbedField{
				Name: "Example:",
				Value: has.Example(),
			})
		}
		arguments := make(map[string]string, 0)
		flags := make(map[string]string, 0)
		options := make(map[string]string, 0)
		for _, param := range cmd.Params() {
			if param.Type == CommandParamTypeArgument {
				arguments[param.Name] = param.Usage
			}
			if param.Type == CommandParamTypeFlag {
				flags[param.Name] = param.Usage
			}
			if param.Type == CommandParamTypeOption {
				options[param.Name] = param.Usage
			}
		}
		details := make([]string, 0)
		customDetails := false
		if has, ok := interface{}(cmd).(ICommandCustomDetails); ok {
			customDetails = true
			details = append(details, has.CustomDetails())
		}
		if customDetails == false {
			var cmdArgs strings.Builder
			var argsUsage strings.Builder
			cmdArgs.WriteString(cmd.Name())
			if len(arguments) > 0 {
				for arg, detail := range arguments {
					cmdArgs.WriteString(" <" + arg + ">")
					if len(detail) > 0 {
						argsUsage.WriteString("  " + arg + ": " + detail + "\n")
					}
				}
			}
			details = append(details, cmdArgs.String(), argsUsage.String())
		}
		if customDetails == false && len(options) > 0 {
			var opts strings.Builder
			opts.WriteString("Options:\n")
			for opt, detail := range options {
				opts.WriteString("  <-" + opt + "=\"value\">")
				if len(detail) > 0 {
					opts.WriteString(" " + detail)
				}
				opts.WriteString("\n")
			}
			details = append(details, opts.String())
		}
		if customDetails == false && len(flags) > 0 {
			var fls strings.Builder
			fls.WriteString("Flags:\n")
			for f, detail := range flags {
				fls.WriteString("  <-" + f + ">")
				if len(detail) > 0 {
					fls.WriteString(" " + detail)
				}
				fls.WriteString("\n")
			}
			details = append(details, fls.String())
		}
		res.Fields = append(res.Fields, &discordgo.MessageEmbedField{
			Name: "Usage Details: ",
			Value: "```md\n" + strings.Join(details, "\n") + "```",
		})
		return res, nil
	} else {
		return &discordgo.MessageEmbed{
			Title: "Invalid Command",
			Description: "I cannot help you with `" + cmdName + "` because that command does not exist.",
		}, fmt.Errorf("command does not exist")
	}
}