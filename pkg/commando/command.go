package commando

import (
	"fmt"
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/gin-gonic/gin"
)

func (b *Bot) RegisterCommands(commands ...ICommand) {
	for _, cmd := range commands {
		b.commandRegistry[cmd.Name()] = cmd
	}
	b.commandRegistry["help"] = NewHelpCmd(b)
	b.Session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID { return }
		parts := strings.SplitN(m.Content, " ", 3)
		if parts[0] == b.Invocation {
			if len(parts) < 2 {
				s.ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
					Description:       "Hello, <@!" + m.Author.ID + ">!",
					Color:       b.Theme,
				})
				return
			}
			if len(parts) < 3 {
				parts = append(parts, "")
			}
			if err := s.MessageReactionAdd(m.ChannelID, m.ID, "\U0001F4AC"); err != nil {
				log.Printf("error attaching reaction: %s\n", err)
			}
			if cmd, ok := b.commandRegistry[parts[1]]; ok {
				permissions := 0
				if has, ok := interface{}(cmd).(ICommandPermissions); ok {
					permissions = has.Permissions()
				}
				if MemberHasPermissions(s, m.GuildID, m.Author.ID, permissions) {
					ctx := &CommandContext{
						Theme:	   b.Theme,
						UserID:    m.Author.ID,
						GuildID:   m.GuildID,
						ChannelID: m.ChannelID,
						Params:    ParamParse(cmd.Params(), parts[2]),
						Session:   s,
					}
					res, err := cmd.Invoke(ctx)
					if res == nil && err != nil {
						res = &discordgo.MessageEmbed{
							Title: 		 "Oof",
							Description: fmt.Sprintf("Something broke. Yell at eKKo.\n\n**info**: %v", err),
						}
						log.Printf("error during invoke: %v", err)
					}
					ctx.Error = err
					if res != nil && b.Theme > 0 {
						res.Color = b.Theme
					}
					if reply, err := s.ChannelMessageSendEmbed(m.ChannelID, res); err != nil {
						log.Printf("error responding to command: %v", err)
					} else {
						ctx.Reply = reply
						if hook, ok := interface{}(cmd).(ICommandAfterReply); ok {
							if err := hook.AfterReply(ctx); err != nil {
								log.Printf("hook failed: %v", err)
							}
						}
					}
				}
			} else {
				s.ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
					Title:       "Invalid Command",
					Description: "There was no valid command, try using the help command: `"+b.Invocation+" help`",
					Color:       b.Theme,
				})
			}
			if err := s.MessageReactionRemove(m.ChannelID, m.ID, "\U0001F4AC", s.State.User.ID); err != nil {
				log.Printf("error removing reaction: %s\n", err)
			}
		}
	})
	for name, cmd := range b.commandRegistry {
		b.router.GET("/command/"+name, HelpApiCommand(cmd))
		b.router.POST("/command/"+name, func(c *gin.Context) {})
	}
}