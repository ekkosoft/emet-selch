package commando

import "github.com/bwmarrin/discordgo"

func MemberHasPermissions(s *discordgo.Session, guildId string, userId string, permission int) bool {
	if permission == 0 { return true }
	member, err := s.State.Member(guildId, userId)
	if err != nil {
		if member, err = s.GuildMember(guildId, userId); err != nil {
			return false
		}
	}

	for _, roleId := range member.Roles {
		role, err := s.State.Role(guildId, roleId)
		if err != nil {
			return false
		}
		if role.Permissions&permission != 0 {
			return true
		}
	}
	return false
}