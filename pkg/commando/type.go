package commando

import (
	"github.com/bwmarrin/discordgo"
)

/**
Basic Types
 */

type ParamDefinition []*CommandParameter

/**
ICommand Interfaces
 */

type ICommand interface {
	Name() string
	Params() ParamDefinition
	Invoke(*CommandContext) (*discordgo.MessageEmbed, error)
}

type ICommandPermissions interface {
	Permissions() int
}

type ICommandDescription interface {
	Description() string
}

type ICommandExample interface {
	Example() string
}

type ICommandAfterReply interface {
	AfterReply(*CommandContext) error
}

type ICommandCustomDetails interface {
	CustomDetails() string
}

/**
Structs
 */

type CommandContext struct {
	Theme int
	UserID string
	GuildID string
	ChannelID string
	Params ParsedParams
	Error error
	Data interface{}
	Session *discordgo.Session
	Reply *discordgo.Message
}
