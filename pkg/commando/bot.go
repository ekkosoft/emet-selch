package commando

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gin-gonic/gin"
)

type Bot struct {
	Invocation       string
	Session          *discordgo.Session
	Theme 			 int
	router			 *gin.Engine
	api 			 *http.Server
	commandRegistry  map[string]ICommand
}

func NewBot(invocation string, token string) (*Bot, error) {
	session, err := discordgo.New("Bot " + token)
	if err != nil {
		return nil, fmt.Errorf("error creating Discord session: %v", err)
	}
	router := gin.New()
	router.Use(gin.Recovery())
	return &Bot{
		Invocation:       invocation,
		Session:          session,
		router:			  router,
		commandRegistry:  make(map[string]ICommand),
	}, nil
}

func (b *Bot) Start(bindAddress string) error {
	err := b.Session.Open()
	if err != nil {
		return fmt.Errorf("error opening Discord connection: %v", err)
	}
	b.api = &http.Server{
		Addr: bindAddress,
		Handler: b.router,
	}
	go func() {
		if err := b.api.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	return nil
}

func (b *Bot) Stop() {
	log.Println("shutting down bot")
	if err := b.Session.Close(); err != nil {
		log.Fatalf("error closing discord connection: %v", err)
	}
	log.Println("shutting down api server...")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 5)
	defer cancel()
	if err := b.api.Shutdown(ctx); err != nil {
		log.Fatalf("api server forced to shutdown: %v", err)
	}
	log.Println("server exiting")
}