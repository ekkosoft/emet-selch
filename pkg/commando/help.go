package commando

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

type HelpSingleCommand struct {
	Name string `json:"name"`
	Description string `json:"description"`
	ApiEndpoint string `json:"apiEndpoint"`
}

func HelpApiCommand(cmd ICommand) func(*gin.Context) {
	return func(c *gin.Context) {
		c.JSON(200, &HelpSingleCommand{
			Name:        cmd.Name(),
			ApiEndpoint: fmt.Sprintf("/command/%s", cmd.Name()),
		})
	}
}
