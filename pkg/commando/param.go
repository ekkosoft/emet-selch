package commando

import (
	"strings"
)

type ParsedParams map[string]string


type CommandParameterType int

type InputParam struct {
	Value string
	Parsed bool
	Type CommandParameterType
}

type CommandParameter struct {
	Name string
	Value string
	Usage string
	Type CommandParameterType
}

const (
	CommandParamTypeArgument = 0
	CommandParamTypeFlag = 1
	CommandParamTypeOption = 2
)

func ParamParse(params ParamDefinition, cmdArgs string) ParsedParams {
	parsed := make(ParsedParams, 0)
	parsed["_raw"] = cmdArgs
	if len(params) < 1 {
		return parsed
	}
	args := ParamSplit(cmdArgs)
	for  _, p := range params {
		parsed[p.Name] = p.Value
		argFound := false
		for _, a := range args {
			if a.Parsed || argFound || a.Type != p.Type { continue }
			if p.Type == CommandParamTypeArgument {
				parsed[p.Name] = a.Value
				argFound = true
				a.Parsed = true
				continue
			}
			if p.Type == CommandParamTypeFlag {
				parsed[p.Name] = "1"
				a.Parsed = true
				continue
			}
			if p.Type == CommandParamTypeOption {
				opts := strings.SplitN(a.Value, "=", 2)
				if p.Name == opts[0] {
					parsed[p.Name] = opts[1]
				}
			}
		}
	}
	return parsed
}

func ParamSplit(cmdArgs string) []*InputParam {
	var (
		startState = 0
		quoteState = 1
		argState = 2
	)
	var args []*InputParam
	state := startState
	var pType CommandParameterType = CommandParamTypeArgument
	var current strings.Builder
	for i := 0; i < len(cmdArgs); i++ {
		char := cmdArgs[i]

		if state == startState && char == '-' {
			pType = CommandParamTypeFlag
			continue
		}

		if state == quoteState {
			if char != '"' {
				current.WriteByte(char)
			} else {
				args = append(args, &InputParam{Value: current.String(), Type: pType})
				current.Reset()
				state = startState
				pType = CommandParamTypeArgument
			}
			continue
		}

		if char == '"' {
			state = quoteState
			continue
		}

		if char == '=' && pType == CommandParamTypeFlag {
			pType = CommandParamTypeOption
			current.WriteByte(char)
			continue
		}

		if state == argState {
			if char == ' ' || char == '\t' || char == '\n' {
				args = append(args, &InputParam{Value: current.String(), Type: pType})
				current.Reset()
				state = startState
				pType = CommandParamTypeArgument
			} else {
				current.WriteByte(char)
			}
			continue
		}

		if char != ' ' && char != '\t' && char != '\n' {
			state = argState
			current.WriteByte(char)
		}
	}

	if current.Len() > 0 {
		args = append(args, &InputParam{Value: current.String(), Type: pType})
		current.Reset()
	}

	return args
}
