package emet_selch

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
)

type __CMDTMP struct {
	commando.ICommand
	params commando.ParamDefinition
}

func New__CMDTMPCmd() *__CMDTMP {
	return &__CMDTMP{params: commando.ParamDefinition{}}
}

func (c *__CMDTMP) Name() string {
	return ""
}

func (c *__CMDTMP) Description() string {
	return ""
}

func (c *__CMDTMP) Params() commando.ParamDefinition {
	return c.params
}

func (c *__CMDTMP) Invoke(_ *commando.CommandContext) (*discordgo.MessageEmbed, error) {
	return &discordgo.MessageEmbed{
		Title: 		 "",
	}, nil
}