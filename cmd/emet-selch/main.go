package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/ekkosoft/emet-selch/command"
	"gitlab.com/ekkosoft/emet-selch/command/classes"
	"gitlab.com/ekkosoft/emet-selch/command/iam"
	"gitlab.com/ekkosoft/emet-selch/command/whoami"
	"gitlab.com/ekkosoft/emet-selch/db"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
	"gitlab.com/ekkosoft/emet-selch/service"
	goLodestone "gitlab.com/ekkosoft/go-lodestone"
)

func main() {
	bot, err := commando.NewBot(os.Getenv("INVOKE"), os.Getenv("BOT_TOKEN"))
	if err != nil {
		log.Fatalf("error creating bot: %v\n", err)
	}

	api, _ := goLodestone.NewApi("na")
	ls := goLodestone.NewLodestone(api)

	dbConn := db.NewDatabaseConnection()
	db.AutoMigrate(dbConn, false)

	bot.Theme = 10038562

	bot.Session.AddHandler(func (s *discordgo.Session, m *discordgo.Ready) {
		s.UpdateListeningStatus("these mortals")
	})

	bot.RegisterCommands(
		command.NewPingCmd(),
		iam.NewIamCmd(service.IamNameService(ls), service.IamSaveService(dbConn)),
		whoami.NewWhoamiCmd(service.WhoamiGetIdService(dbConn), service.WhoamiLodestoneService(ls)),
		classes.NewClassesCmd(service.ClassesGetIdService(dbConn), service.ClassesLodestoneService(ls)),
		command.NewPollCmd(),
		command.NewReactRolesCmd(),
	)

	if err = bot.Start(":4000"); err != nil {
		log.Fatalf("error starting bot: %v\n", err)
		return
	}

	fmt.Println("Emet-Selch is online.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	bot.Stop()
}
