module gitlab.com/ekkosoft/emet-selch

go 1.14

require (
	github.com/bwmarrin/discordgo v0.21.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.14
	gitlab.com/ekkosoft/go-lodestone v0.0.2
)
