package command

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
)

type Poll struct {
	commando.ICommand
	params commando.ParamDefinition
}

func NewPollCmd() *Poll {
	return &Poll{params: commando.ParamDefinition{}}
}

func (c *Poll) Name() string {
	return "poll"
}

func (c *Poll) Description() string {
	return "Create a simple poll using reactions"
}

func (c *Poll) Example() string {
	return "`poll What's for dinner?; Tacos; Hot Dogs`"
}

func (c *Poll) Params() commando.ParamDefinition {
	return c.params
}

func (c *Poll) CustomDetails() string {
	return "poll question?; option 1; option 1; etc"
}

func (c *Poll) Invoke(ctx *commando.CommandContext) (*discordgo.MessageEmbed, error) {
	input := strings.Split(ctx.Params["_raw"], ";")
	if len(input) < 2 {
		return &discordgo.MessageEmbed{
			Title: 		 "Invalid Input",
			Description: "Make sure you have a question and at least one option, separated by semi-colons.\n" +
			"Ex- " + c.Example(),
		}, fmt.Errorf("invalid input")
	}
	hex := 127462
	options := make([]string, 0)
	for i, opt := range input[1:] {
		options = append(options, string(hex+i) + " " + strings.Trim(opt, " "))
	}
	return &discordgo.MessageEmbed{
		Description: "<@!" + ctx.UserID + "> Asks:",
		Fields: 	 []*discordgo.MessageEmbedField{
			{
				Name: strings.Trim(input[0], " "),
				Value: strings.Join(options, "\n"),
			},
		},
	}, nil
}

func (c *Poll) AfterReply(ctx *commando.CommandContext) error {
	if ctx.Error != nil { return nil }
	hex := 127462
	opts := strings.Split(ctx.Params["_raw"], ";")
	for i, _ := range opts[1:] {
		if err := ctx.Session.MessageReactionAdd(ctx.ChannelID, ctx.Reply.ID, string(hex+i)); err != nil {
			fmt.Printf("error attaching reaction: %s\n", err)
		}
	}
	return nil
}