package command

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
)

type ReactRoles struct {
	commando.ICommand
	params commando.ParamDefinition
}

func NewReactRolesCmd() *ReactRoles {
	return &ReactRoles{
		params: commando.ParamDefinition{
			&commando.CommandParameter{
				Name:  "action",
				Value: "",
				Usage: "What action to take",
				Type:  commando.CommandParamTypeArgument,
			},
		},
	}
}

func (c *ReactRoles) Name() string {
	return "reactrole"
}

func (c *ReactRoles) Description() string {
	return "Sets up a message with reactions that can toggle user roles."
}

func (c *ReactRoles) Permissions() int {
	return discordgo.PermissionAdministrator
}

func (c *ReactRoles) Params() commando.ParamDefinition {
	return c.params
}

func (c *ReactRoles) Invoke(ctx *commando.CommandContext) (*discordgo.MessageEmbed, error) {
	if len(ctx.Params["action"]) == 0 {
		return &discordgo.MessageEmbed{
			Title: "Missing Action",
			Description: "You must include an action!",
		}, fmt.Errorf("no action included")
	}
	return &discordgo.MessageEmbed{
		Title: "React Roles",
		Description: "yay we did the thing",
	}, nil
}