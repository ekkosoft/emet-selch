package whoami

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
)

type Whoami struct {
	commando.ICommand
	params            commando.ParamDefinition
	getSavedCharacter GetSavedCharacterId
	getLodestoneData  GetLodestoneData
}

type GetSavedCharacterId func(userId string, isAlt bool) (string, error)

type GetLodestoneData func(lodestoneId string) (*Character, error)

type Character struct {
	ID string
	FacePortrait string
	Title string
	FullName string
	World string
	Race string
	Clan string
	Gender string
	Nameday string
	Guardian string
	CityState string
	GrandCompany string
	FreeCompany string
	FullPortrait string
}

func NewWhoamiCmd(idService GetSavedCharacterId, lodestoneService GetLodestoneData) *Whoami {
	return &Whoami{
		getSavedCharacter: idService,
		getLodestoneData: lodestoneService,
		params: commando.ParamDefinition{
			&commando.CommandParameter{
				Name:  "alt",
				Value: "",
				Usage: "Look up alt character data",
				Type:  commando.CommandParamTypeFlag,
			},
		},
	}
}

func (c *Whoami) Name() string {
	return "whoami"
}

func (c *Whoami) Description() string {
	return "Gets your character profile from the Lodestone."
}

func (c *Whoami) Params() commando.ParamDefinition {
	return c.params
}

func (c *Whoami) Invoke(ctx *commando.CommandContext) (*discordgo.MessageEmbed, error) {
	isAlt := len(ctx.Params["alt"]) > 0
	lsId, err := c.getSavedCharacter(ctx.UserID, isAlt)
	if err != nil {
		return &discordgo.MessageEmbed{
			Title: 		 "Character Not Saved",
			Description: "You don't have character data saved.",
		}, fmt.Errorf("character not saved: %v", err)
	}
	char, err := c.getLodestoneData(lsId)
	if err != nil {
		return &discordgo.MessageEmbed{
			Title: 		 "Character Not Found",
			Description: "Your character data couldn't be found on Lodestone. (This can be caused by Lodestone maintenance)",
		}, fmt.Errorf("character not found: %v", err)
	}
	return &discordgo.MessageEmbed{
		Title:       char.FullName,
		Thumbnail:   &discordgo.MessageEmbedThumbnail{URL: char.FullPortrait},
		//Image: &discordgo.MessageEmbedImage{URL: char.FullPortrait},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  "World / Title",
				Value:  char.World + " / " + fmtMissing(char.Title),
			},
			{
				Name:  "Race / Clan / Gender",
				Value: char.Race + " / " + char.Clan + " / " + char.Gender,
			},
			{
				Name: "Nameday",
				Value: char.Nameday,
			},
			{
				Name: "Guardian",
				Value: char.Guardian,
			},
			{
				Name: "Grand Company",
				Value: fmtMissing(char.GrandCompany),
			},
			{
				Name: "Free Company",
				Value: fmtMissing(char.FreeCompany),
			},
		},
	}, nil
}

func fmtMissing(info string) string {
	if len(info) > 0 {
		return info
	}
	return "(n/a)"
}