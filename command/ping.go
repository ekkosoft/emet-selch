package command

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
)

type Ping struct {
	commando.ICommand
	params commando.ParamDefinition
}

func NewPingCmd() *Ping {
	return &Ping{params: commando.ParamDefinition{}}
}

func (c *Ping) Name() string {
	return "ping"
}

func (c *Ping) Description() string {
	return "A simple ping command which should reply 'Pong!'"
}

func (c *Ping) Params() commando.ParamDefinition {
	return c.params
}

func (c *Ping) Invoke(_ *commando.CommandContext) (*discordgo.MessageEmbed, error) {
	return &discordgo.MessageEmbed{
		Title: 		 "Pong!",
	}, nil
}