package classes

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
)

type Classes struct {
	commando.ICommand
	params            commando.ParamDefinition
	getSavedCharacter GetSavedCharacterId
	getLodestoneData  GetLodestoneData
}

type GetSavedCharacterId func(userId string, isAlt bool) (string, error)

type GetLodestoneData func(lodestoneId string) (*Character, error)

type Character struct {
	ID string
	FullName string
	FullPortrait string
	Tanks map[string]string
	Healers map[string]string
	DPS map[string]string
	DOH map[string]string
	DOL map[string]string
}

func NewClassesCmd(idService GetSavedCharacterId, lodestoneService GetLodestoneData) *Classes {
	return &Classes{
		getSavedCharacter: idService,
		getLodestoneData: lodestoneService,
		params: commando.ParamDefinition{
			&commando.CommandParameter{
				Name:  "alt",
				Value: "",
				Usage: "Look up alt character classes",
				Type:  commando.CommandParamTypeFlag,
			},
		},
	}
}

func (c *Classes) Name() string {
	return "classes"
}

func (c *Classes) Description() string {
	return "Gets your character classes from the Lodestone."
}

func (c *Classes) Params() commando.ParamDefinition {
	return c.params
}

func (c *Classes) Invoke(ctx *commando.CommandContext) (*discordgo.MessageEmbed, error) {
	isAlt := len(ctx.Params["alt"]) > 0
	lsId, err := c.getSavedCharacter(ctx.UserID, isAlt)
	if err != nil {
		return &discordgo.MessageEmbed{
			Title: 		 "Character Not Saved",
			Description: "You don't have character data saved.",
		}, fmt.Errorf("character not saved: %v", err)
	}
	char, err := c.getLodestoneData(lsId)
	if err != nil {
		return &discordgo.MessageEmbed{
			Title: 		 "Character Not Found",
			Description: "Your character data couldn't be found on Lodestone. (This can be caused by Lodestone maintenance)",
		}, fmt.Errorf("character not found: %v", err)
	}
	return &discordgo.MessageEmbed{
		Title:       char.FullName,
		Thumbnail:   &discordgo.MessageEmbedThumbnail{URL: char.FullPortrait},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  "Tanks",
				Value: fmtClasses(char.Tanks),
				Inline: true,
			},
			{
				Name: "Healers",
				Value: fmtClasses(char.Healers),
				Inline: true,
			},
			{
				Name: "DPS",
				Value: fmtClasses(char.DPS),
			},
			{
				Name: "DoH",
				Value: fmtClasses(char.DOH),
				Inline: true,
			},
			{
				Name: "DoL",
				Value: fmtClasses(char.DOL),
				Inline: true,
			},
		},
	}, nil
}

func fmtClasses(classes map[string]string) string {
	var (
		output strings.Builder
		l1 strings.Builder
		l2 strings.Builder
	)
	maxLen := 6
	i := 0
	output.WriteString("```\n")
	for class, level := range classes {
		l1.WriteString(class + " ")
		if len(level) < 2 {
			l2.WriteString(" " + level + "  ")
		} else {
			l2.WriteString(level + "  ")
		}
		i++
		if i >= maxLen {
			output.WriteString(l1.String() + "\n" + l2.String() + "\n")
			l1.Reset()
			l2.Reset()
			i = 0
		}
	}
	output.WriteString(l1.String() + "\n" + l2.String() + "\n")
	output.WriteString("```")
	return output.String()
}