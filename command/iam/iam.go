package iam

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ekkosoft/emet-selch/pkg/commando"
)

type Iam struct {
	commando.ICommand
	params commando.ParamDefinition
	getCharacterByName FindByNameService
	saveCharacter SaveCharacter
}

type FindByNameService func(string,string,string) (*Character, error)

type SaveCharacter func(string, string, bool) error

type Character struct {
	ID string
	Name string
	Surname string
	World string
	FreeCompany string
	Portrait string
}

type CmdData struct {
	IsAlt bool
	Character *Character
}

func NewIamCmd(nameService FindByNameService, characterService SaveCharacter) *Iam {
	return &Iam{
		saveCharacter: characterService,
		getCharacterByName: nameService,
		params: commando.ParamDefinition{
			&commando.CommandParameter{
				Name:         "world",
				Value:        "",
				Usage:        "World name",
				Type:         commando.CommandParamTypeArgument,
			},
			&commando.CommandParameter{
				Name:         "name",
				Value:        "",
				Usage:        "Character first name",
				Type:         commando.CommandParamTypeArgument,
			},
			&commando.CommandParameter{
				Name:         "surname",
				Value:        "",
				Usage:        "Character last name",
				Type:         commando.CommandParamTypeArgument,
			},
			&commando.CommandParameter{
				Name:  "alt",
				Value: "",
				Usage: "Mark this is as an alt character",
				Type:  commando.CommandParamTypeFlag,
			},
		},
	}
}

func (c *Iam) Name() string {
	return "iam"
}

func (c *Iam) Description() string {
	return "Searches lodestone for your character information and saves it"
}

func (c *Iam) Example() string {
	return "`" + c.Name() + " Adamantoise Karen Needsamanager`\n`" + c.Name() + " -id=12345678`"
}

func (c *Iam) Params() commando.ParamDefinition {
	return c.params
}

func (c *Iam) Invoke(ctx *commando.CommandContext) (*discordgo.MessageEmbed, error) {
	isAlt := len(ctx.Params["alt"]) > 0
	if len(ctx.Params["world"]) < 1 || len(ctx.Params["name"]) < 1 || len(ctx.Params["surname"]) < 1 {
		return &discordgo.MessageEmbed{
			Title: 		 "Invalid Input",
			Description: "You must include the world name, and your character first and last name.",
		}, fmt.Errorf("invalid input")
	}
	char, err := c.getCharacterByName(ctx.Params["world"], ctx.Params["name"], ctx.Params["surname"])
	if err != nil {
		return &discordgo.MessageEmbed{
			Title: 		 "Character Not Found",
			Description: "Make sure the name and world are spelled correctly.",
		}, fmt.Errorf("character not found")
	}
	if c.saveCharacter(ctx.UserID, char.ID, isAlt) != nil {
		return &discordgo.MessageEmbed{
			Title: 		 "Character Save Error",
			Description: "Your character was found, but it could not be saved successfully.",
		}, fmt.Errorf("character not saved")
	}
	if len(char.FreeCompany) < 1 {
		char.FreeCompany = "(n/a)"
	}
	desc := "Your character data has been saved."
	if isAlt {
		desc = "Your alternate character data has been saved. Because this is an alt, your nickname has not been changed."
	} else {
		if err := ctx.Session.GuildMemberNickname(ctx.GuildID, ctx.UserID, char.Name+" "+char.Surname); err != nil {
			desc += " Your nickname could not be updated. This is likely due to my role being too low in the hierarchy."
		} else {
			desc += " Your nickname has been updated."
		}
	}
	ctx.Data = &CmdData{
		IsAlt:     isAlt,
		Character: char,
	}
	return &discordgo.MessageEmbed{
		Description: desc,
		Thumbnail:   &discordgo.MessageEmbedThumbnail{URL: char.Portrait},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  char.Name + " " + char.Surname + " of " + char.World,
				Value: "Free Company: " + char.FreeCompany,
			},
		},
	}, nil
}